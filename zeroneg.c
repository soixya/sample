#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: zeroneg.alg		*/
/* Compiled at Thu Jul 12 12:25:59 2018		*/


#include	<stdio.h>
#include "zeroneg.h"

//	Code for the global declarations

int _a_40; /* a declared at line 4*/
int  A_jff_0A (char *LP, int V){
return _a_40 = V;
}
int  _jff_0A (char *LP, int d){
return _a_40;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 4
ininteger (0, LP, A_jff_0A, _jff_0A);
 if ((_a_40) > (0) )
 outstring (1, "Positive\n");
 
 else {  if ((_a_40) < (0) )
 outstring (1, "Negative\n");
 
 else outstring (1, "Zero\n");
}
}
}
