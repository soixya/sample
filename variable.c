#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: variable.alg		*/
/* Compiled at Thu Jul 12 12:17:10 2018		*/


#include	<stdio.h>
#include "variable.h"

//	Code for the global declarations

int _a_40; /* a declared at line 2*/
int _b_40; /* b declared at line 2*/
double _c_40; /* c declared at line 3*/
int  A_jff_0A (char *LP, int V){
return _a_40 = V;
}
int  _jff_0A (char *LP, int d){
return _a_40;
}
double  A_jff_1A (char *LP, double V){
return _c_40 = V;
}
double  _jff_1A (char *LP, int d){
return _c_40;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
ininteger (0, LP, A_jff_0A, _jff_0A);
inreal (0, LP, A_jff_1A, _jff_1A);
_b_40=5;
 if ((_a_40) > (0) )
 _b_40=_a_40;
 
 else _b_40= -(_a_40);
outinteger (1, _a_40);
outinteger (1, _b_40);
outreal (1, _c_40);
newline (1);
outinteger (1, (int)(((double)(((__ipow(_a_40,2))) + ((3) * (_b_40)))) +  (_c_40)));
}
}
