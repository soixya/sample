#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: factorial.alg		*/
/* Compiled at Thu Jul 12 12:47:43 2018		*/


#include	<stdio.h>
#include "factorial.h"

//	Code for the global declarations

 /* factorial declared at line 2*/
int _factorial_41 (char	*Ln, int (*An)( char *, int), int (*Vn)(char *, int)){ 
 struct ___factorial_41_rec local_data_factorial;
struct ___factorial_41_rec *LP = & local_data_factorial;
LP -> Ln = Ln;
LP -> An = An;
LP -> Vn = Vn;

{ // code for block at line 3
 (LP ) -> __res_val =  ( (((LP) -> Vn)(((LP) -> Ln), 0)) < (1)  )? 1 : (_factorial_41 (LP, A_jff_0A, _jff_0A)) * (((LP) -> Vn)(((LP) -> Ln), 0));
}
return LP -> __res_val;

}
int  A_jff_0A (char *LP, int V){
fault (" no assignable object",4);
}
int  _jff_0A (char *LP, int d){
return (((((struct ___factorial_41_rec *)(LP))) -> Vn)(((((struct ___factorial_41_rec *)(LP))) -> Ln), 0)) - (1);
}
int  A_jff_1A (char *LP, int V){
fault (" no assignable object",7);
}
int  _jff_1A (char *LP, int d){
return 5;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
outinteger (1, _factorial_41 (LP, A_jff_1A, _jff_1A));
}
}
