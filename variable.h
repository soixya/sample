#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: variable.alg		*/
/* Compiled at Thu Jul 12 12:17:10 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	void outreal (int,double); /* outreal declared at line 43*/
extern	void ininteger (int,char	*,int (*)(char *, int), int(*)(char *, int)); /* ininteger declared at line 45*/
extern	void inreal (int,char	*,double (*)(char *, double), double(*)(char *, int)); /* inreal declared at line 46*/
extern int _a_40; /* a declared at line 2*/
extern int _b_40; /* b declared at line 2*/
extern double _c_40; /* c declared at line 3*/

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);

//	specification for thunk
extern double A_jff_1A (char *, double);
extern double _jff_1A (char *, int);
