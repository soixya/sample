#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: zeroneg.alg		*/
/* Compiled at Thu Jul 12 12:25:59 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void ininteger (int,char	*,int (*)(char *, int), int(*)(char *, int)); /* ininteger declared at line 45*/
extern int _a_40; /* a declared at line 4*/

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);
