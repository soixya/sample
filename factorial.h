#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: factorial.alg		*/
/* Compiled at Thu Jul 12 12:47:43 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern	int _factorial_41 (char	*,int (*)(char *, int), int(*)(char *, int)); /* factorial declared at line 2*/
struct ___factorial_41_rec {
char *__l_field;
int __res_val;
char	*Ln;
 int (*An)(char *, int);
int (*Vn)(char *, int);
};

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);

//	specification for thunk
extern int A_jff_1A (char *, int);
extern int _jff_1A (char *, int);
