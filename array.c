#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: array.alg		*/
/* Compiled at Thu Jul 12 12:35:18 2018		*/


#include	<stdio.h>
#include "array.h"

//	Code for the global declarations

int _a_40[30 - 20 +1]; /* a declared at line 2*/
int __dv0 [2 * 2 + DOPE_BASE];
int	*_b_40; /* b declared at line 3*/
double _c_40[0 -  -(10) +1]; /* c declared at line 4*/
int _i_40; /* i declared at line 5*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1)_a_40 [_i_40-20]=(_i_40) * (_i_40);
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1)outinteger (1, _a_40 [_i_40-20]);
}
}
